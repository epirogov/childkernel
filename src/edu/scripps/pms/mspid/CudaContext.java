package edu.scripps.pms.mspid;

import jcuda.*;
import jcuda.driver.*;

import java.io.*;

import static jcuda.driver.JCudaDriver.*;
import static jcuda.driver.JCudaDriver.cuInit;

/**
 * Created by rpark on 4/12/17.
 */
public final class CudaContext {
    private CudaContext(){
        // maybe here need to call init method ?
    }

    private static CUdevice device;
    private static CUcontext context;

     /**
     * The module which is loaded in form of a PTX file
     */
    private static CUmodule module;

    /**
     * The actual kernel function from the module
     */
    private static CUfunction function;
    private static CUfunction countPeptides;
    private static CUfunction helloworld;
    private static CUfunction calcXScore3;

    /**
     * Temporary memory for the device output
     */
    private static CUdeviceptr deviceBuffer;


    public static String ptxFileName = "";

    public static CUdevice getDevice() {
        return device;
    }

    public static void setDevice(CUdevice device) {
        CudaContext.device = device;
    }

    public static CUcontext getContext() {
        return context;
    }

    public static void setContext(CUcontext context) {
        CudaContext.context = context;
    }

    public static CUmodule getModule() {
        return module;
    }

    public static void setModule(CUmodule module) {
        CudaContext.module = module;
    }

    public static CUfunction getFunction() {
        return function;
    }

    public static void setFunction(CUfunction function) {
        CudaContext.function = function;
    }

    public static CUfunction getCountPeptidesFunction() {
        return countPeptides;
    }

    public static void setCountPeptidesFunction(CUfunction function) {
        CudaContext.countPeptides = function;
    }


    public static CUdeviceptr getDeviceBuffer() {
        return deviceBuffer;
    }

    public static void setDeviceBuffer(CUdeviceptr deviceBuffer) {
        CudaContext.deviceBuffer = deviceBuffer;
    }

    public static void init() throws IOException
    {
        cuInit(0);
        CUdevice device = new CUdevice();
        cuDeviceGet(device, 0);
        context = new CUcontext();
        cuCtxCreate(context, 0, device);
        prepare();
        // Enable exceptions and omit all subsequent error checks
        JCudaDriver.setExceptionsEnabled(true);
    }

    public static void prepare() throws IOException
    {
        // Load the module from the PTX file
        module = new CUmodule();
        ptxFileName = preparePtxFile("/home/paperspace/childkernel/src/helloWord.cu");
        System.out.println("ptx file " + ptxFileName);
        int ret = cuModuleLoad(module, ptxFileName);
        System.out.println("Cuda module " + ret);

        // Obtain a function pointer to the "xCorrPreprocessKernel" function.
        function = new CUfunction();
        helloworld = new CUfunction();
        int getHelloworld = cuModuleGetFunction(helloworld,module,"helloWorld");
        // Allocate a chunk of temporary memory (must be at least
        // numberOfBlocks * Sizeof.FLOAT)
        deviceBuffer = new CUdeviceptr();
        cuMemAlloc(deviceBuffer, 1024 * Sizeof.FLOAT);
        System.out.println("Cuda initialized helloworld function with number " + getHelloworld);

    }

    public static String preparePtxFile(String cuFileName) throws IOException
    {
        int endIndex = cuFileName.lastIndexOf('.');
        if (endIndex == -1)
        {
            endIndex = cuFileName.length()-1;
        }
        String ptxFileName = cuFileName.substring(0, endIndex+1)+"ptx";
        File ptxFile = new File(ptxFileName);
        if (ptxFile.exists())
        {
            return ptxFileName;
        }

        File cuFile = new File(cuFileName);
        if (!cuFile.exists())
        {
            throw new IOException("Input file not found: "+cuFileName);
        }
        //"  -gencode=arch=compute_30,code=sm_30" +
//"  -gencode=arch=compute_35,code=sm_35" +
//"  -gencode=arch=compute_50,code=sm_50" +
//"  -gencode=arch=compute_52,code=sm_52" +
//"  -gencode=arch=compute_52,code=compute_52
        String modelString = 
" -gencode=arch=compute_52,code=compute_52 -rdc=true -lcudadevrt";
        String command =
                "/usr/local/cuda/bin/nvcc "+modelString+" -ptx "+
                        cuFile.getPath()+" -o "+ptxFileName;

        System.out.println("Executing\n"+command);
        Process process = Runtime.getRuntime().exec(command);

        String errorMessage =
                new String(toByteArray(process.getErrorStream()));
        String outputMessage =
                new String(toByteArray(process.getInputStream()));
        int exitValue = 0;
        try
        {
            exitValue = process.waitFor();
        }
        catch (InterruptedException e)
        {
            Thread.currentThread().interrupt();
            throw new IOException(
                    "Interrupted while waiting for nvcc output", e);
        }

        if (exitValue != 0)
        {
            System.out.println("nvcc process exitValue "+exitValue);
            System.out.println("errorMessage:\n"+errorMessage);
            System.out.println("outputMessage:\n"+outputMessage);
            throw new IOException(
                    "Could not create .ptx file: "+errorMessage);
        }

        System.out.println("Finished creating PTX file");
        return ptxFileName;
    }

    public static byte[] toByteArray(InputStream inputStream)
            throws IOException
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte buffer[] = new byte[8192];
        while (true)
        {
            int read = inputStream.read(buffer);
            if (read == -1)
            {
                break;
            }
            baos.write(buffer, 0, read);
        }
        return baos.toByteArray();
    }

    public static void shutdown()
    {
        cuModuleUnload(module);
        cuMemFree(deviceBuffer);
        if (context != null)
        {
            cuCtxDestroy(context);
        }
    }

    public static CUfunction getHelloworld() {
        return helloworld;
    }

    public static CUfunction getCalcXScore3() {
        return calcXScore3;
    }
}
