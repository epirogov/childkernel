/*
 * JCudaUtils - Utilities for JCuda
 *
 * Copyright (c) 2010-2016 Marco Hutter - http://www.jcuda.org
 */
package edu.scripps.pms.mspid;

import java.io.IOException;
import static jcuda.driver.JCudaDriver.*;

import java.util.Arrays;

import jcuda.*;
import jcuda.driver.*;
import jcuda.utils.KernelLauncher;

/**
 * A sample demonstrating how the KernelLauncher class may
 * be used to compile inlined source code and execute a
 * kernel function from the source code.
 * https://github.com/jcuda/jcuda-utils/blob/master/JCudaUtils/src/test/java/jcuda/utils/test/KernelLauncherSample.java
 */
public class CudaContextSample
{
    /**
     * Entry point of the sample
     *
     * @param args Not used
     */
    public static void main(String args[]) throws IOException
    {
        JCudaDriver.setExceptionsEnabled(true);
        CudaContext.init();
        cuCtxSetCurrent(CudaContext.getContext());
        
//cuCtxSetCurrent(CudaContext.getContext());
        
        float [] array = new float[1];
        CUdeviceptr M2zInput = new CUdeviceptr();
        cuMemAlloc(M2zInput, 1 * Sizeof.FLOAT);
        cuMemcpyHtoD(M2zInput, Pointer.to(array),
                1 * Sizeof.FLOAT);
        
        cuLaunchKernel(CudaContext.getHelloworld(),
                1, 1, 1,         // Grid dimension
                1, 1, 1,         // Block dimension
                6 * Sizeof.INT, null,   // Shared memory size and stream
                Pointer.to(Pointer.to(M2zInput)), null // Kernel- and extra parameters
        );
        cuCtxSynchronize();
        CudaContext.shutdown();
        
        
    }
}