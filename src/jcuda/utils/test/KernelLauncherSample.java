/*
 * JCudaUtils - Utilities for JCuda
 *
 * Copyright (c) 2010-2016 Marco Hutter - http://www.jcuda.org
 */
package jcuda.utils.test;

import static jcuda.driver.JCudaDriver.*;

import java.util.Arrays;

import jcuda.*;
import jcuda.driver.*;
import jcuda.utils.KernelLauncher;

/**
 * A sample demonstrating how the KernelLauncher class may
 * be used to compile inlined source code and execute a
 * kernel function from the source code.
 * https://github.com/jcuda/jcuda-utils/blob/master/JCudaUtils/src/test/java/jcuda/utils/test/KernelLauncherSample.java
 */
public class KernelLauncherSample
{
    /**
     * Entry point of the sample
     *
     * @param args Not used
     */
    public static void main(String args[])
    {
        int size = 10;
        JCudaDriver.setExceptionsEnabled(true);

        String sourceCode =
            "extern \"C\"" + "\n" +
            "__global__ void helloWorld()" + "\n" +
            "{}" + "\n"+
"__global__ void kernel_parent()" + "\n" + 
                "{"
                + "helloWorld<<<1,1>>>(); "
                + "}";

        // Prepare the kernel
        System.out.println("Preparing the KernelLauncher...");
        KernelLauncher kernelLauncher =
            KernelLauncher.compile(sourceCode, "kernel_parent", "arch=compute_35", "rdc=true");

        // Call the kernel
        System.out.println("Calling the kernel...");
        kernelLauncher.setBlockSize(size, 1, 1);
        kernelLauncher.call();
    }
}