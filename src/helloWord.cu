extern "C"
#include <stdio.h>

__device__ int v = 0;

__global__ void child_k(void) {
  printf("v = %d\n", v);
}

__global__ void helloWorld(void) {
  v = 1;
  child_k <<< 1, 1 >>> ();
  v = 2; // RACE CONDITION
  cudaDeviceSynchronize();
}
